const { Cam, NotificationMessage, CamOptions } = require('onvif');

const TOPIC = /RuleEngine\/CellMotionDetector\/Motion$/;

class MotionDetector {
  lastIsMotion = false;

  constructor(cam) {
      this.cam= cam;
  }

  static async create(options) {
    return new Promise((resolve, reject) => {
      const cam = new Cam(options, (error) => {
        if (error) {
          reject(error);
        } else {
          const monitor = new MotionDetector(cam);
          resolve(monitor);
        }
      });
    });
  }

  listen(onMotion) {
      let falseRecieved = false;
      let timer = null;

    this.cam.on('event', (message) => {
      if (message?.topic?._?.match(TOPIC)) {
        const motion = message.message.message.data.simpleItem.$.Value;

        clearTimeout(timer);
        falseRecieved = falseRecieved || !motion;

        if (motion !== this.lastIsMotion) {
          this.lastIsMotion = motion;
          onMotion(motion);
        }

          if (motion && !falseRecieved) {
              timer = setTimeout(() => {
                  this.lastIsMotion = false;
                  onMotion(false, true);
              }, 2000);
          }
      }
    });
  }

  close() {
    this.cam.removeAllListeners('event');
  }
}

let options = {
    hostname: process.argv[2],
    username: process.argv[3],
    password: process.argv[4],
    port: process.argv[5]
};

function formatDate (late) {
    let date = new Date();

    if (late) {
        date = new Date(date.getTime() - 2000);
    }

    return date.getFullYear() + '.' + (date.getMonth() + 1) + '.' + date.getDate() + ' ' +
           date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ':' + date.getMilliseconds();
}


async function startMotion () {
    const detector = await MotionDetector.create(options);

    console.log("connected");

    detector.listen((motion, late) => {
        if (motion) {
             console.log(formatDate(late), '+>>');
        } else {
             console.log(formatDate(late), '<<-');
        }
    });
}

async function startTimer () {
    console.log('-');
    setTimeout(startTimer, 1000);
}

startMotion();
startTimer();
