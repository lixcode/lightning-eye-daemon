#ifndef DAEMON_H
#define DAEMON_H

#include "dir.h++"
#include "settings.h++"

#include <QDateTime>
#include <QDir>
#include <QProcess>



class OnvifExtractor;

class Daemon : public QObject
{
    const Camera & cam;

    QProcess ffmpeg{this};
    QProcess affmpeg{this};
    QProcess node{this};
    QString  persistentPath;
    QString  continuePath;
    QString  segmentPath;
    QString  errorPath;
    QString  outputPath;
    QString  aSegmentPath;
    QString  aErrorPath;
    QString  aOuPutPath;
    QString  nodeOutPath;
    Dir *    persistent = nullptr;
    Dir *    motion     = nullptr;
    Dir *    onvif      = nullptr;

    bool onvifMotionActive = false;
    bool nodeProcessReload = false;
    bool ffmpegWasClosed   = false;
    bool nodeWasClosed     = false;

    QDateTime lastPathUpdateTime;

    OnvifExtractor * onvifExtractor = nullptr;

public:
    Daemon(const Camera & cam);

private:
    void switchFFmeg(bool state);
    void pauseFfmpeg();
    void playFfmpeg();

    void createContinuePath();

    QString getPathByTime(int secs = 0);

    QStringList prepareArgs(QList<QStringList> data);

protected slots:
    void runPersistentFfmpeg();
    void runAlternativeFfmpeg();

    void motionWatch();
    void onvifWatch();

public slots:
    void run();
};

#endif  // DAEMON_H
