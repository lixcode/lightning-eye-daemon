/*
 * @file FTPClient.h
 * @brief libcurl wrapper for FTP requests
 *
 * @author Mohamed Amine Mzoughi <mohamed-amine.mzoughi@laposte.net>
 * @date 2017-01-17
 */

#ifndef INCLUDE_FTPCLIENT_H_
#define INCLUDE_FTPCLIENT_H_

#define FTPCLIENT_VERSION "FTPCLIENT_VERSION_1.0.0"

#include "curl.h++"

#include <QDateTime>
#include <QMutex>
#include <QString>

#include <algorithm>
#include <atomic>
#include <cstddef>  // std::size_t
#include <cstdio>   // snprintf
#include <cstdlib>
#include <cstring>  // strerror, strlen, memcpy, strcpy
#include <ctime>
#include <curl/curl.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory>  // std::unique_ptr
#include <mutex>
#include <sstream>
#include <stdarg.h>  // va_start, etc.
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>

struct SFile
{
    QString name;
    qint64  size;
};

class Ftp
{
public:
    enum class FTP_PROTOCOL : unsigned char { FTP, SFTP };

    explicit Ftp();

    // copy constructor and assignment operator are disabled
    Ftp(const Ftp &) = delete;
    Ftp & operator=(const Ftp &) = delete;

    // allow constructor and assignment operator are disabled
    Ftp(Ftp &&)   = delete;
    Ftp & operator=(Ftp &&) = delete;

    // Setters - Getters (for unit tests)
    inline void setTimeout(const int & value) {
        m_curlTimeout = value;
    }
    inline void setActive(const bool & value) {
        m_active = value;
    }
    inline void setNoSignal(const bool & value) {
        m_noSignal = value;
    }
    inline void setInsecure(const bool & value) {
        m_insecure = value;
    }
    inline const int getTimeout() const {
        return m_curlTimeout;
    }
    inline const unsigned getPort() const {
        return m_port;
    }
    inline const bool getActive() {
        return m_active;
    }
    inline const bool getNoSignal() const {
        return m_noSignal;
    }
    inline const bool getInsecure() const {
        return m_insecure;
    }

    // Session
    const bool initSession(
      const QString & host, const unsigned & port, const QString & username,
      const QString & password, const FTP_PROTOCOL & protocol);

    // FTP requests
    const bool createDir(const QString & base, const QString & path) const;

    const bool removeDir(const QString & dir) const;

    const bool removeFile(const QString & remoteFile) const;

    QList<SFile> list(const QString & remoteFolder, const QChar & type) const;

private:
    /* common operations are performed here */
    inline const CURLcode perform() const;

    // Curl callbacks
    static size_t writeInStringCallback(
      void * ptr, size_t size, size_t nmemb, void * data);
    static size_t ThrowAwayCallback(
      void * ptr, size_t size, size_t nmemb, void * data);

    QString m_username;
    QString m_password;
    QString m_host;
    QString m_baseUrl;

    bool     m_active;  // For active FTP connections
    bool     m_noSignal;
    bool     m_insecure;
    unsigned m_port;

    FTP_PROTOCOL m_ftpProtocol;

    CURL * m_curlSession;
    int    m_curlTimeout;

    Curl & m_curl;
};

#endif
