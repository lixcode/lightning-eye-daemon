#include "curl.h++"

#include <curl/curl.h>
#include <stdexcept>

Curl::Curl() {
    const auto eCode = curl_global_init(CURL_GLOBAL_ALL);
    if (eCode != CURLE_OK) {
        throw std::runtime_error{"Error initializing libCURL"};
    }
}

Curl::~Curl() {
    curl_global_cleanup();
}

Curl & Curl::instance() {
    static Curl inst{};
    return inst;
}
