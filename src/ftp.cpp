/**
 * @file FTPClient.cpp
 * @brief implementation of the FTP client class
 * @author Mohamed Amine Mzoughi <mohamed-amine.mzoughi@laposte.net>
 */

#include "ftp.h++"

#include <QBuffer>
#include <QList>
#include <QMap>
#include <QMutex>
#include <QRegularExpression>
#include <QtDebug>

#include <algorithm>
#include <iterator>
#include <stdexcept>

// Static members initialization

#ifdef DEBUG_CURL
std::string CFTPClient::s_strCurlTraceLogDirectory;
#endif

/**
 * @brief constructor of the FTP client object
 *
 * @param Logger - a callabck to a logger function void(const std::string&)
 *
 */
Ftp::Ftp()
    : m_curlTimeout(0)
    , m_port(0)
    , m_ftpProtocol(FTP_PROTOCOL::FTP)
    , m_active(false)
    , m_noSignal(true)
    , m_insecure(false)
    , m_curlSession(nullptr)
    , m_curl(Curl::instance()) {}

static QMap<QString, CURL *> curlSessions;

QMutex mutex;

/**
 * @brief starts a new FTP session, initializes the cURL API session
 *
 * if a new session was already started, the method has no effect.
 *
 * @param [in] strHost server address
 * @param [in] uPort the remote port
 * @param [in] strLogin username
 * @param [in] strPassword password
 * @param [in] eFtpProtocol the protocol used to connect to the server (FTP,
 * SFTP)
 *
 * @retval true   Successfully initialized the session.
 * @retval false  The session is already initialized.
 * call CleanupSession() before initializing a new one or the Curl API is not
 * initialized.
 */
const bool Ftp::initSession(
  const QString & host, const unsigned & port, const QString & username,
  const QString & password, const FTP_PROTOCOL & protocol) {
    if (m_curlSession) {
        return false;
    }

    mutex.lock();

    m_host        = host;
    m_port        = port;
    m_username    = username;
    m_password    = password;
    m_ftpProtocol = protocol;
    m_baseUrl =
      (protocol == FTP_PROTOCOL::FTP ? "ftp://" : "sftp://") + host + "//";

    if (curlSessions.contains(m_baseUrl)) {
        m_curlSession = curlSessions[m_baseUrl];
    }
    else {
        m_curlSession = curl_easy_init();
        curlSessions.insert(m_baseUrl, m_curlSession);
    }

    mutex.unlock();

    return (m_curlSession != nullptr);
}

/**
 * @brief creates a remote directory
 *
 * @param [in] strNewDir the remote direcotry to be created.
 *
 * @retval true   Successfully created a directory.
 * @retval false  The directory couldn't be created.
 */
const bool Ftp::createDir(const QString & base, const QString & path) const {

    mutex.lock();

    // Reset is mandatory to avoid bad surprises
    curl_easy_reset(m_curlSession);

    struct curl_slist * headerlist = nullptr;

    QString remoteFolder;
    bool    bRet    = false;
    auto    headers = QList<QByteArray>{};

    if (m_ftpProtocol == FTP_PROTOCOL::SFTP) {
        remoteFolder = m_baseUrl;
        auto arr     = path.split('/');
        auto buff    = QString{};

        for (const auto & folder : arr) {
            headers.append(("mkdir /" + base + buff + folder).toUtf8());
            buff += folder + '/';
        }

        curl_easy_setopt(m_curlSession, CURLOPT_POSTQUOTE, headerlist);
    }
    else {
        remoteFolder =
          m_baseUrl + QString{base + path + '/'}.replace("/", "//");
    }

    for (const auto & header : headers) {
        headerlist = curl_slist_append(headerlist, header.constData());
    }

    auto utf8Path = remoteFolder.toUtf8();

    // Specify target
    curl_easy_setopt(m_curlSession, CURLOPT_URL, utf8Path.constData());

    curl_easy_setopt(m_curlSession, CURLOPT_POSTQUOTE, headerlist);
    curl_easy_setopt(m_curlSession, CURLOPT_NOBODY, 1L);
    curl_easy_setopt(m_curlSession, CURLOPT_HEADER, 1L);
    curl_easy_setopt(
      m_curlSession, CURLOPT_FTP_CREATE_MISSING_DIRS, CURLFTP_CREATE_DIR);

    auto r = perform();
    bRet   = r == CURLE_OK;

    // clean up the FTP commands list
    curl_slist_free_all(headerlist);

    mutex.unlock();

    return bRet;
}

/**
 * @brief removes an empty remote directory
 *
 * if the remote directory ain't empty, the method will fail.
 * yhe user must use RemoveFile() on all directory's files then use RemoveDir to
 * delete the latter.
 *
 * @param [in] strDir the remote direcotry to be removed.
 *
 * @retval true   Successfully removed a directory.
 * @retval false  The directory couldn't be removed.
 */
const bool Ftp::removeDir(const QString & strDir) const {

    mutex.lock();

    // Reset is mandatory to avoid bad surprises
    curl_easy_reset(m_curlSession);

    struct curl_slist * headerlist = nullptr;

    QString strRemoteFolder;
    QString strRemoteFolderName;
    QString strBuf;
    bool    bRet = false;

    if (m_ftpProtocol == FTP_PROTOCOL::SFTP) {
        strRemoteFolder     = m_baseUrl;
        strRemoteFolderName = strDir;
        strBuf += "rmdir /";
    }
    else {
        int uFound = strDir.lastIndexOf("/", strDir.length() - 2);
        if (uFound != -1) {
            strRemoteFolder = strDir.mid(0, uFound).replace("/", "//") + "//";
            strRemoteFolderName = strDir.mid(uFound + 1);
        }
        else {
            strRemoteFolder     = m_baseUrl;
            strRemoteFolderName = strDir;
        }

        // Append the rmd command
        strBuf += "RMD ";
    }

    auto utf8Path = strRemoteFolder.toUtf8();

    // Specify target
    curl_easy_setopt(m_curlSession, CURLOPT_URL, utf8Path.constData());

    strBuf += strRemoteFolderName;
    headerlist = curl_slist_append(headerlist, strBuf.toUtf8());

    curl_easy_setopt(m_curlSession, CURLOPT_POSTQUOTE, headerlist);
    curl_easy_setopt(m_curlSession, CURLOPT_NOBODY, 1L);
    curl_easy_setopt(m_curlSession, CURLOPT_HEADER, 1L);

    CURLcode res = perform();

    bRet = res == CURLE_OK;

    // clean up the FTP commands list
    curl_slist_free_all(headerlist);

    mutex.unlock();

    return bRet;
}

/**
 * @brief deletes a remote file
 *
 * @param [in] strRemoteFile the URL of the remote file
 *
 * @retval true   Successfully deleted the file.
 * @retval false  The file couldn't be deleted.
 */
const bool Ftp::removeFile(const QString & strRemoteFile) const {

    mutex.lock();

    // Reset is mandatory to avoid bad surprises
    curl_easy_reset(m_curlSession);

    struct curl_slist * headerlist = nullptr;

    QString strRemoteFolder;
    QString strRemoteFileName;
    QString strBuf;

    bool bRet = false;

    if (m_ftpProtocol == FTP_PROTOCOL::SFTP) {
        strRemoteFolder   = m_baseUrl;
        strRemoteFileName = strRemoteFile;

        // Append the rm command
        strBuf += "rm /";
    }
    else {
        // Splitting file name
        int uFound = strRemoteFile.lastIndexOf("/");
        if (uFound != -1) {
            strRemoteFolder = m_baseUrl +
                              strRemoteFile.mid(0, uFound).replace("/", "//") +
                              "//";
            strRemoteFileName = strRemoteFile.mid(uFound + 1);
        }
        else  // the file to be deleted is located in the root directory
        {
            strRemoteFolder   = m_baseUrl;
            strRemoteFileName = strRemoteFile;
        }

        // Append the delete command
        strBuf += "DELE ";
    }

    auto utf8path = strRemoteFolder.toUtf8();
    curl_easy_setopt(m_curlSession, CURLOPT_URL, utf8path.constData());

    strBuf += strRemoteFileName;
    headerlist = curl_slist_append(headerlist, strBuf.toUtf8());

    curl_easy_setopt(m_curlSession, CURLOPT_POSTQUOTE, headerlist);
    curl_easy_setopt(m_curlSession, CURLOPT_NOBODY, 1L);
    curl_easy_setopt(m_curlSession, CURLOPT_HEADER, 1L);

    CURLcode res = perform();

    bRet = res == CURLE_OK;

    // clean up the FTP commands list
    curl_slist_free_all(headerlist);

    mutex.unlock();

    return bRet;
}

/**
 * @brief lists a remote folder
 * the list can contain only names or can be detailed
 * entries/names will be separated with LF ('\n')
 *
 * @param [in] strRemoteFolder URL of the remote location to be listed
 * @param [out] strList will contain the directory entries (detailed or not)
 * @param [in] bOnlyNames detailed list or only names
 *
 */
QList<SFile> Ftp::list(const QString & folder, const QChar & type) const {
    QString strList;
    mutex.lock();

    // Reset is mandatory to avoid bad surprises
    curl_easy_reset(m_curlSession);

    bool bRet = false;

    auto strRemoteFile =
      (m_baseUrl + QString{folder}.replace("/", "//")).toUtf8();

    curl_easy_setopt(m_curlSession, CURLOPT_URL, strRemoteFile.constData());

    curl_easy_setopt(
      m_curlSession, CURLOPT_WRITEFUNCTION, writeInStringCallback);
    curl_easy_setopt(m_curlSession, CURLOPT_WRITEDATA, &strList);

    CURLcode res = perform();

    if (res != CURLE_OK) {
        return {};
    }

    mutex.unlock();

    auto list  = strList.split("\n");
    auto regex = QRegularExpression{
      "^[\\w-]{10}\\s+\\d+\\s\\w+\\s+\\w+\\s+(?<size>\\d+)\\s(\\w{3}\\s+\\d+"
      "\\s+(\\d{4}|\\d{2}:\\d{2}))\\s+(?<name>'.*'|\\S+)\\s*$"};

    QList<SFile> ret;

    for (const auto & str : list) {
        if (!str.isEmpty() && str[0] != type) {
            continue;
        }

        auto match = regex.match(str);

        if (match.hasMatch()) {
            auto name = match.captured("name");

            if (name[0] != '.') {
                ret.append({name, match.captured("size").toLongLong()});
            }
        }
    }

    std::sort(ret.begin(), ret.end(), [](const SFile & s1, const SFile & s2) {
        return s1.name < s2.name;
    });

    return ret;
}

/**
 * @brief performs the chosen FTP request
 * sets up the common settings (Timeout, proxy,...)
 *
 *
 * @retval true   Successfully performed the request.
 * @retval false  The request couldn't be performed.
 *
 */
const CURLcode Ftp::perform() const {
    auto res   = CURLE_OK;
    auto login = (m_username + ":" + m_password).toUtf8();

    curl_easy_setopt(m_curlSession, CURLOPT_PORT, m_port);
    curl_easy_setopt(m_curlSession, CURLOPT_USERPWD, login.constData());
    curl_easy_setopt(m_curlSession, CURLOPT_TCP_KEEPALIVE, 1L);

    if (m_active)
        curl_easy_setopt(m_curlSession, CURLOPT_FTPPORT, "-");

    if (m_curlTimeout > 0) {
        curl_easy_setopt(m_curlSession, CURLOPT_TIMEOUT, m_curlTimeout);
    }
    if (m_noSignal) {
        curl_easy_setopt(m_curlSession, CURLOPT_NOSIGNAL, 1L);
    }


    // Perform the requested operation
    res = curl_easy_perform(m_curlSession);

    return res;
}

// CURL CALLBACKS

size_t Ftp::ThrowAwayCallback(
  void * ptr, size_t size, size_t nmemb, void * data) {
    /* we are not interested in the headers itself,
    so we only return the size we would have saved ... */
    return size * nmemb;
}

/**
 * @brief stores the server response in a string
 *
 * @param ptr pointer of max size (size*nmemb) to read data from it
 * @param size size parameter
 * @param nmemb memblock parameter
 * @param data pointer to user data (string)
 *
 * @return (size * nmemb)
 */
size_t Ftp::writeInStringCallback(
  void * ptr, size_t size, size_t nmemb, void * data) {
    QString * strWriteHere = reinterpret_cast<QString *>(data);
    if (strWriteHere != nullptr) {
        strWriteHere->insert(0, reinterpret_cast<char *>(ptr));
        return size * nmemb;
    }
    return 0;
}
