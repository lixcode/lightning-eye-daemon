#ifndef SETTINGS_H
#define SETTINGS_H

#include <QList>
#include <QString>

extern const QString appDir;
extern const QString confPath;

struct Camera
{
    int id = 0;

    bool enabled          = false;
    bool audioEnabled     = false;
    bool persistentEnabed = false;
    bool motionEnabled    = false;
    bool alternative      = false;
    bool onvifEnabled     = false;

    QString url{};
    QString ext{};
    QString alternativeUrl{};

    bool regionEnabled = false;
    int  regionX = 0, regionY = 0;
    int  regionWidth = 0, regionHeight = 0;

    QString persistentPath{};
    QString motionPath{};
    QString onvifPath{};

    int persistentQuota = 0;
    int motionQuota     = 0;
    int onvifQuota      = 0;

    QString onvifHostname{};
    QString onvifUsername{};
    QString onvifPassword{};
    int     onvifPort{};
    float   onvifDelay{};

    float beforeS      = 0;
    float afterS       = 0;
    float minThreshold = 0.0075;
    float maxThreshold = 0.04;
    float minNoise     = 0;
    float maxNoise     = 0;

    int framesToStep    = 15;
    int framesToSegment = 1;
    int segmentsToStart = 2;
    int segmentsToEnd   = 10;
    int shiftCount      = 2;

    int   segmentDuration = 300;
    float motionDuration  = 2;
};

class Settings
{
public:
    int     port   = 17411;
    QString login  = "admin";
    QString passwd = "admin";

    QList<Camera> sources;

public:
    Settings();

    void load();

    void save();

    void createNodeModule();
};

#endif  // SETTINGS_H
