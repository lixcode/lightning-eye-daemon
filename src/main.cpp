#include "analyser.h++"
#include "daemon.h++"
#include "extractor.h++"
#include "motion.h++"
#include "settings.h++"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QMutex>
#include <QTextStream>
#include <QThread>

void myMessageOutput(
  QtMsgType type, const QMessageLogContext & context, const QString & msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    auto       currTime =
      QDateTime::currentDateTime().toString("ddd hh:mm:ss").toUtf8();

    static int    debugCount = 0;
    static QMutex mutex;

    mutex.lock();

    switch (type) {

    case QtDebugMsg:
        fprintf(
          stderr,
          debugCount % 2 == 0 ? "[42m[30m%s[49m[32m %s[37m\n"
                              : "[44m[30m%s[49m[34m %s[37m\n",
          currTime.constData(), localMsg.constData());
        debugCount++;
        break;
    case QtInfoMsg:
        fprintf(
          stdout, "[47m[30m%s[49m[37m %s\n", currTime.constData(),
          localMsg.constData());
        debugCount = 0;
        break;

    default:
        break;
    }

    mutex.unlock();
}

int main(int argc, char * argv[]) {
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Lightning eye");
    QCoreApplication::setApplicationVersion("1.0");
    qInstallMessageHandler(myMessageOutput);

    QCommandLineParser parser;
    parser.setApplicationDescription(
      "Record video from camera and detect motion with small CPU usage");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOptions(
      {{{"c", "create-config"}, "Create config files"},
       {{"a", "analyse"},
        "Analyse a video to get threshold of motion.",
        "path"},
       {{"m", "motion"}, "Analyse a video to get intervals of motion.", "path"},
       {{"e", "extract"},
        "Analyse a video to extract fragments with motion.",
        "path"},
       {{"i", "id"}, "Id of camera to test"}});

    parser.process(a);

    if (parser.isSet("create-config")) {
        Settings settings;

        settings.load();
        settings.save();
        return 0;
    }

    Settings settings;
    int      id = 0;

    if (parser.isSet("id")) {
        int id = parser.value("id").toInt();
        settings.load();

        if (id < 0 && id >= settings.sources.length()) {
            qInfo("Camera not found");
            return 1;
        }
    }
    if (parser.isSet("analyse") && parser.isSet("id")) {
        QString path = parser.value("analyse");

        auto data = Analyser::run(settings.sources[id], path);

        QFile       fileOut("analyze" + QString::number(id) + ".csv");
        QTextStream out(&fileOut);

        fileOut.open(QFile::WriteOnly);

        for (const auto & item : data) {
            out << item.time << ';' << item.score << '\n';
        }

        qInfo("\nAnalyzation complete successfully");
        fileOut.close();
        return 0;
    }
    if (parser.isSet("motion") && parser.isSet("id")) {
        QString path = parser.value("motion");

        auto data = Motion::run(settings.sources[id], path);

        for (auto & interval : data) {
            qInfo("interval [%f, %f]", interval.begin, interval.end);
        }

        qInfo("\nMotion complete successfully");
    }
    if (parser.isSet("extract") && parser.isSet("id")) {
        QString path = parser.value("extract");

        Dir dir{nullptr};
        Extractor::run(
          settings.sources[id], dir, path, QDir::currentPath() + '/');
        qInfo("\nExtraction complete successfully");
        return 0;
    }

    settings.load();

    for (auto & cam : settings.sources) {
        auto daemon = new Daemon(cam);
        auto thread = new QThread(&a);

        daemon->moveToThread(thread);
        thread->start();

        QMetaObject::invokeMethod(daemon, [=]() { daemon->run(); });

        qInfo("Daemon %i started", cam.id);
    }
    qInfo("All daemons started");

    return a.exec();
}
