#ifndef DIR_H
#define DIR_H

#include "ftp.h++"

#include <QDateTime>
#include <QDir>
#include <QList>
#include <QProcess>
#include <QTextStream>



class Ftp;

class Daemon;

class Dir
{
    QDir dir;

    enum class Modes { Local, Ftp };

    Modes   mode = Modes::Local;
    QString mpath, fpath;

    QString login, password, host, ipath, port;
    Ftp *   ftp;

    QObject * parent;

    struct BDir
    {
        QString name;
    };

    struct HDir : BDir
    {
        QList<SFile> files;
    };

    struct DDir : BDir
    {
        QList<HDir> dirs;
    };

    struct YmDir : BDir
    {
        QList<DDir> dirs;
    };

    QList<YmDir> dirs;

    qint64 size  = 0;
    qint64 quota = 0;

public:
    Dir(QObject * parent);

    QString path() const;

    void setPath(const QString & path);

    bool mkpath(QString path);
    bool rmDir(QString path);
    bool rmFile(QString path, bool internal = false);

    bool addFile(QString path);

    void setQuota(qint64 value);
    void ensureSpace();

    QDateTime creationTime(QString name);

    bool checkForMissing(QStringList & in, QStringList & ain);

private:
    void _init();
    void __init();

    QStringList _missings();
    QStringList __missings();

    bool _mkPath(const QString & path);
    bool __mkPath(const QString & path);

    bool _rmDir(const QString & path);
    bool __rmDir(const QString & path);

    bool _rmFile(const QString & path);
    bool __rmFile(const QString & path);

    template <typename T>
    T * check(QList<T> & list, const QString & name);

    HDir * findH(QString path);
};

#endif  // DIR_H
