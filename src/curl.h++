#ifndef INCLUDE_CURLHANDLE_H_
#define INCLUDE_CURLHANDLE_H_

class Curl
{
public:
    static Curl & instance();

    Curl(Curl const &) = delete;
    Curl(Curl &&)      = delete;

    Curl & operator=(Curl const &) = delete;
    Curl & operator=(Curl &&) = delete;

    ~Curl();

private:
    Curl();
};

#endif
