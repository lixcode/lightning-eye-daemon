#include "daemon.h++"

#include "extractor.h++"
#include "onvifextractor.h++"

#include <QDateTime>
#include <QDebug>
#include <QMetaObject>
#include <QMutex>
#include <QStorageInfo>
#include <QTextStream>
#include <QThread>
#include <QTimer>

Daemon::Daemon(const Camera & cam)
    : cam(cam) {
    lastPathUpdateTime = QDateTime::currentDateTime().addDays(-1);

    const auto base = QDir::homePath() + '/' + appDir + '/';

    segmentPath  = base + "p-segment" + QString::number(cam.id);
    aSegmentPath = base + "a-segment" + QString::number(cam.id);

    errorPath  = base + "p-" + QString::number(cam.id) + "-error";
    aErrorPath = base + "a-" + QString::number(cam.id) + "-error";

    outputPath = base + "p-" + QString::number(cam.id) + "-output";
    aOuPutPath = base + "a-" + QString::number(cam.id) + "-output";

    connect(
      &ffmpeg, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this,
      [=]() {
          ffmpegWasClosed = true;
          qInfo("ffmpeg restarted %i", cam.id);
          affmpeg.close();
      });
    connect(
      &affmpeg, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this,
      [=]() {
          qInfo("aleternative ffmpeg restarted %i", cam.id);
          affmpeg.start();
      });
    connect(
      &node, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this,
      [=]() {
          nodeWasClosed = true;
          qInfo("node restarted %i", cam.id);
      });
}

void Daemon::createContinuePath() {
    if (lastPathUpdateTime.secsTo(QDateTime::currentDateTime()) < 300) {
        return;
    }

    auto path = getPathByTime(300);

    if (path != continuePath) {
        persistent->mkpath(path);
        continuePath = path;
    }

    lastPathUpdateTime = QDateTime::currentDateTime();
}

QString Daemon::getPathByTime(int secs) {
    return QDateTime::currentDateTime().addSecs(secs).toString(
      "yyyy-MM/dd/hh/");
}

QStringList Daemon::prepareArgs(QList<QStringList> data) {
    QStringList ret;

    for (const auto & list : data) {
        ret.append(list);
    }

    return ret;
}

void Daemon::runPersistentFfmpeg() {
    const auto args = prepareArgs(
      {{"-use_wallclock_as_timestamps", "1"},
       {"-analyzeduration", "1000000"},
       {"-probesize", "1000000"},
       {"-fflags", "+igndts"},
       {"-rtsp_transport", "tcp"},
       {"-i", cam.url},
       cam.audioEnabled ? QStringList{"-acodec", "copy"} : QStringList{"-an"},
       {"-vcodec", "copy"},
       {"-movflags", "+faststart"},
       {"-t", QString::number(12 * 60 * 60)},
       {"-f", "segment"},
       {"-strftime", "1"},
       {"-segment_atclocktime", "1"},
       {"-reset_timestamps", "1"},
       {"-segment_list", segmentPath},
       {"-segment_time", QString::number(cam.segmentDuration)},
       {persistent->path() + "/%Y-%02m/%02d/%02H/%02M-%02S" + cam.ext}});

    qDebug("run persistent ffmpeg %s", args.join(" ").toUtf8().data());

    ffmpeg.setProgram("ffmpeg");
    ffmpeg.setArguments(args);
    ffmpeg.setStandardErrorFile(errorPath, QProcess::WriteOnly);
    ffmpeg.setStandardOutputFile(outputPath, QProcess::WriteOnly);

    ffmpeg.start(QProcess::WriteOnly);
    ffmpeg.waitForStarted();
}

void Daemon::runAlternativeFfmpeg() {
    QStringList args = prepareArgs(
      {{"-use_wallclock_as_timestamps", "1"},
       {"-analyzeduration", "1000000"},
       {"-probesize", "1000000"},
       {"-fflags", "+igndts"},
       {"-rtsp_transport", "tcp"},
       {"-i", cam.alternativeUrl},
       {"-an"},
       {"-vcodec", "copy"},
       {"-movflags", "+faststart"},
       {"-t", QString::number(12 * 60 * 60)},
       {"-f", "segment"},
       {"-segment_atclocktime", "1"},
       {"-reset_timestamps", "1"},
       {"-strftime", "1"},
       {"-segment_list", aSegmentPath},
       {"-segment_time", QString::number(cam.segmentDuration)},
       {persistent->path() + "/%Y-%02m/%02d/%02H/%02M-%02S-a" + cam.ext}});

    qDebug("run alternative ffmpeg %s", args.join(" ").toUtf8().data());

    affmpeg.setProgram("ffmpeg");
    affmpeg.setArguments(args);
    affmpeg.setStandardErrorFile(aErrorPath, QProcess::ReadWrite);
    affmpeg.setStandardOutputFile(aOuPutPath, QProcess::ReadWrite);

    affmpeg.start(QProcess::ReadWrite);
}

void Daemon::motionWatch() {
    QFile       file(segmentPath);
    QFile       afile(aSegmentPath);
    QTextStream in(&file);
    QTextStream ain(&afile);

    if (
      !file.open(QFile::ReadOnly) ||
      (cam.alternative && cam.motionEnabled && !afile.open(QFile::ReadOnly))) {
        qInfo("Failed to open segment/err file for camera %i", cam.id);
        return;
    }

    QString str, astr;

    qint64 lastErrSize = 0, lastNodeSize = 0;

    QFileInfo errFileInfo(errorPath);
    QFileInfo nodeFileInfo(nodeOutPath);
    QDateTime lastRefresh = QDateTime::currentDateTime();

    QStringList inList, ainList;

    while (true) {

        if (str.isNull()) {
            str = in.readLine();
        }

        if (astr.isNull() && cam.alternative && cam.motionEnabled) {
            astr = ain.readLine();
        }

        if (
          !str.isNull() &&
          (!cam.alternative || !cam.motionEnabled ||
           (cam.alternative && cam.motionEnabled && !astr.isNull())) &&
          persistent->checkForMissing(inList, ainList)) {

            if (
              cam.alternative && cam.motionEnabled &&
              inList.length() != ainList.length()) {
                continue;
            }

            for (int i = 0; i < inList.length(); i++) {

                auto path  = inList[i].split('/').mid(0, 3).join('/') + '/';
                auto input = persistent->path() + '/' + inList[i];

                qInfo(
                  "New video from camera %i ready: %s", cam.id,
                  inList[i].toUtf8().data());

                persistent->ensureSpace();

                str.replace(cam.ext, "");

                if (cam.motionEnabled) {
                    motion->ensureSpace();
                    motion->mkpath(path);
                    Extractor::run(cam, *motion, input, path + str);
                }

                if (cam.onvifEnabled) {
                    onvif->ensureSpace();
                    onvif->mkpath(path);

                    if (nodeWasClosed) {
                        onvifExtractor->reopenFile();
                    }

                    onvifExtractor->run(cam, *onvif, input, path + str);
                }

                if (!cam.persistentEnabed) {
                    persistent->rmFile(inList[i]);
                }

                if (cam.alternative && cam.motionEnabled) {
                    persistent->rmFile(ainList[i]);
                }
            }

            inList.clear();
            ainList.clear();

            str.clear();
            astr.clear();
            continue;
        }

        if (ffmpegWasClosed) {
            file.close();
            file.open(QFile::ReadOnly);
            in.seek(0);

            if (cam.alternative && cam.motionEnabled) {
                afile.close();
                afile.open(QFile::ReadOnly);
                ain.seek(0);
            }

            ffmpegWasClosed = false;
            ffmpeg.start();
            ffmpeg.waitForStarted();
        }

        if (nodeWasClosed) {
            nodeWasClosed = false;
            lastNodeSize  = 0;
            node.start();
            node.waitForStarted();

            while (true) {
                nodeFileInfo.refresh();

                if (lastNodeSize != nodeFileInfo.size()) {
                    break;
                }
            }
        }

        if (lastRefresh.secsTo(QDateTime::currentDateTime()) > 5) {
            errFileInfo.refresh();
            if (lastErrSize == errFileInfo.size()) {
                ffmpeg.close();

                if (cam.alternative && cam.motionEnabled) {
                    affmpeg.close();
                }
            }
            lastErrSize = errFileInfo.size();

            nodeFileInfo.refresh();
            if (lastNodeSize == nodeFileInfo.size() && cam.onvifEnabled) {
                if (!nodeWasClosed) {
                    node.close();
                }
            }
            lastNodeSize = nodeFileInfo.size();

            lastRefresh = QDateTime::currentDateTime();
        }

        createContinuePath();

        QThread::sleep(1);
    }
}

void Daemon::onvifWatch() {
    QProcess npm{this};
    QString  dir = QDir::homePath() + '/' + appDir + "/node";

    npm.setWorkingDirectory(dir);
    npm.setProgram("npm");
    npm.setArguments({"i"});

    static QMutex mutex;
    static bool   npmRunned = false;

    mutex.lock();
    if (!npmRunned) {
        npmRunned = true;
        npm.start();
        npm.waitForStarted();
        npm.waitForFinished();
    }
    mutex.unlock();

    nodeOutPath = dir + QString::number(cam.id) + ".txt";

    node.setProgram("node");
    node.setArguments(
      {dir + "/index.js", cam.onvifHostname, cam.onvifUsername,
       cam.onvifPassword, QString::number(cam.onvifPort)});
    node.setStandardOutputFile(nodeOutPath, QProcess::Append);

    qDebug("start node %s", node.arguments().join(' ').toUtf8().data());

    QFile::remove(nodeOutPath);
    node.start();
    node.waitForStarted();

    onvifExtractor = new OnvifExtractor(nodeOutPath);
}

void Daemon::run() {
    if (!cam.enabled) {
        return;
    }
    const qint64 multiplier = 1024 * 1024 * 1024;

    persistentPath = getPathByTime();

    if (cam.persistentEnabed) {
        persistent = new Dir{this};
        persistent->setPath(cam.persistentPath);
        persistent->setQuota(qint64(cam.persistentQuota) * multiplier);
    }

    if (cam.motionEnabled) {
        motion = new Dir{this};
        motion->setPath(cam.motionPath);
        motion->setQuota(qint64(cam.motionQuota) * multiplier);

        if (persistent == nullptr) {
            persistent = motion;
        }
    }

    if (cam.onvifEnabled) {
        onvif = new Dir{this};
        onvif->setPath(cam.onvifPath);
        onvif->setQuota(qint64(cam.onvifQuota) * multiplier);

        if (persistent == nullptr) {
            persistent = onvif;
        }
    }

    if (!persistent->mkpath(persistentPath)) {
        qInfo("failed to make persistent path for camera %i", cam.id);
        return;
    }

    createContinuePath();
    runPersistentFfmpeg();

    if (cam.alternative && cam.motionEnabled) {
        runAlternativeFfmpeg();
        affmpeg.waitForStarted();
    }
    ffmpeg.waitForStarted();

    if (cam.onvifEnabled) {
        onvifWatch();
    }
    if (cam.motionEnabled || cam.onvifEnabled) {
        QThread::sleep(5);
        motionWatch();
    }
}
