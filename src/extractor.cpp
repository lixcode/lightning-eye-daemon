#include "extractor.h++"

#include "motion.h++"

#include <QDebug>
#include <QMutex>
#include <QProcess>
#include <QRegularExpression>

Extractor::Extractor() {}

void Extractor::run(
  const Camera & cam, Dir & dir, const QString & path, QString outputPath) {
    auto intervals = Motion::run(cam, path);

    extract(intervals, cam, dir, path, outputPath);
}

void Extractor::extract(
  const QList<Interval> & intervals, const Camera & cam, Dir & dir,
  const QString & path, QString outputPath) {

    static QMutex mutex;

    mutex.lock();

    auto timeRegex = QRegularExpression{"/(?<m>\\d{2})\\-(?<s>\\d{2})$"};
    auto match     = timeRegex.match(outputPath);
    auto m = 0, s = 0;

    if (match.hasMatch()) {
        m = match.captured("m").toInt();
        s = match.captured("s").toInt();
        outputPath.replace(QRegExp("\\d+-\\d+$"), "");
    }

    for (const auto & interval : intervals) {
        QProcess process;

        int  begin     = int(interval.begin) + m * 60 + s;
        int  end       = int(interval.end) + m * 60 + s;
        auto writePath = dir.path() + '/' + outputPath +
                         formatInt(QString::number(begin / 60)) + ':' +
                         formatInt(QString::number(begin % 60)) + '-' +
                         formatInt(QString::number(end / 60)) + ':' +
                         formatInt(QString::number(end % 60)) + cam.ext;

        process.setProgram("ffmpeg");
        process.setArguments(
          {"-i", path, "-ss", QString::number(interval.begin, 'f', 2), "-t",
           QString::number(interval.end - interval.begin, 'f', 2), "-c", "copy",
           writePath});

        qDebug(
          "run extact ffmpeg %s",
          process.arguments().join(' ').toUtf8().data());

        process.start();
        process.waitForStarted();
        process.waitForFinished();

        dir.addFile(writePath);
    }

    mutex.unlock();
}

QString Extractor::formatInt(const QString & in) {
    return in.rightJustified(2, '0');
}
