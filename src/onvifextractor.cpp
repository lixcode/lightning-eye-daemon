#include "onvifextractor.h++"

#include "extractor.h++"
#include "motion.h++"

#include <QRegularExpression>
#include <QtMath>

OnvifExtractor::OnvifExtractor(const QString & path) {
    file.setFileName(path);
    file.open(QFile::ReadOnly);
}

void OnvifExtractor::run(
  const Camera & cam, Dir & dir, const QString & path, QString outputPath) {

    QRegularExpression on{
      "^(\\d+)\\.(\\d+)\\.(\\d+)\\s(\\d+):(\\d+):(\\d+):(\\d+)\\s\\+>>$"};
    QRegularExpression off{
      "^(\\d+)\\.(\\d+)\\.(\\d+)\\s(\\d+):(\\d+):(\\d+):(\\d+)\\s<<\\-$"};

    while (!file.atEnd()) {
        auto line = file.readLine();

        if (line[0] == '-') {
            continue;
        }

        auto isOn  = on.match(line);
        auto isOff = off.match(line);

        if (isOn.hasMatch()) {
            beginTime = extractDate(isOn);
            lastWasOn = true;
        }
        if (isOff.hasMatch()) {
            fragments.append({beginTime, extractDate(isOff)});
            lastWasOn = false;
        }
    }

    QProcess ffprobe;

    ffprobe.setProgram("ffprobe");
    ffprobe.setArguments(
      {"-v", "error", "-show_entries", "format=duration", "-of",
       "default=noprint_wrappers=1:nokey=1", path});

    ffprobe.start();
    ffprobe.waitForStarted();
    ffprobe.waitForReadyRead();

    auto duration = ffprobe.readAllStandardOutput().toFloat();

    QDateTime creation = dir.creationTime(path);
    QDateTime finished = QDateTime{creation}.addMSecs(duration * 1000);

    QList<Interval> intervals;
    QList<Fragment> newFragments;

    if (lastWasOn) {
        fragments.append({beginTime, finished});
    }

    for (auto & fragment : fragments) {

        fragment.begin = fragment.begin.addMSecs(-cam.onvifDelay * 1000.f);
        fragment.end   = fragment.end.addMSecs(-cam.onvifDelay * 1000.f);

        if (fragment.begin < creation) {
            if (fragment.end < creation) {
                continue;
            }

            fragment.begin = creation;
        }

        if (fragment.end > finished) {
            if (fragment.begin > finished) {
                newFragments.append(fragment);
                continue;
            }

            auto clone = fragment;

            clone.begin = finished;
            newFragments.append(clone);
            fragment.end = finished;
        }

        intervals.append(
          {qMax(
             float(creation.msecsTo(fragment.begin)) / 1000.f - cam.beforeS,
             0.f),
           qMax(
             float(creation.msecsTo(fragment.end)) / 1000.f + cam.afterS,
             0.f)});
    }

    fragments.clear();
    fragments.append(newFragments);

    QList<Interval> mergedIntervals;

    if (intervals.length() > 0) {
        auto current = intervals[0];

        for (int i = 1; i < intervals.length(); i++) {
            const auto & canditate = intervals[i];

            if (canditate.begin - current.end > 0) {
                mergedIntervals.append(current);
                current = canditate;
            }
            else {
                current.end = canditate.end;
            }
        }
        mergedIntervals.append(current);
    }

    Extractor::extract(mergedIntervals, cam, dir, path, outputPath);
}

void OnvifExtractor::reopenFile() {
    file.close();
    file.open(QFile::ReadOnly);
    file.seek(0);
}

QDateTime OnvifExtractor::extractDate(const QRegularExpressionMatch & match) {
    QDateTime dt;
    QDate     d;
    QTime     t;

    d.setDate(
      match.captured(1).toInt(), match.captured(2).toInt(),
      match.captured(3).toInt());
    t.setHMS(
      match.captured(4).toInt(), match.captured(5).toInt(),
      match.captured(6).toInt(), match.captured(7).toInt());

    dt.setDate(d);
    dt.setTime(t);

    return dt;
}
