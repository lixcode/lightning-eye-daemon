#include "settings.h++"

#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantHash>

Settings::Settings() {}

extern const QString appDir   = QStringLiteral(".lightning_eye");
extern const QString confPath = "/" + appDir + "/conf.json";

void Settings::load() {
    QDir  home = QDir::home();
    QFile file(home.path() + confPath);

    QJsonDocument   doc;
    QJsonObject     root;
    QJsonParseError err;

    home.mkdir(appDir);

    if (file.open(QFile::ReadOnly)) {
        doc = QJsonDocument::fromJson(file.readAll(), &err);

        if (err.error == QJsonParseError::ParseError::NoError) {
            root = doc.object();
        }
        file.close();
    }

    if (root.contains("port")) {
        port = root["port"].toInt();
    }
    if (root.contains("login")) {
        login = root["login"].toString();
    }
    if (root.contains("passwd")) {
        passwd = root["passwd"].toString();
    }

    if (!root.contains("sources")) {
        return;
    }
    auto arr = root["sources"].toArray();
    auto id  = 0;

    for (const auto & item : arr) {
        const auto obj = item.toObject();
        Camera     camera;

        camera.id = id++;

        if (obj.contains("enabled")) {
            camera.enabled = obj["enabled"].toBool();
        }
        if (obj.contains("url")) {
            camera.url = obj["url"].toString();
        }
        if (obj.contains("ext")) {
            camera.ext = obj["ext"].toString();
        }
        if (obj.contains("audio")) {
            const auto audio = obj["audio"].toObject();

            camera.audioEnabled = audio["enabled"].toBool();
        }
        if (obj.contains("persistent")) {
            const auto persistent = obj["persistent"].toObject();

            camera.persistentEnabed = persistent["enabled"].toBool();
            camera.persistentPath   = persistent["path"].toString();
            camera.persistentQuota  = persistent["quota"].toInt();
        }
        if (obj.contains("motion")) {
            const auto motion = obj["motion"].toObject();

            camera.motionEnabled  = motion["enabled"].toBool();
            camera.motionPath     = motion["path"].toString();
            camera.beforeS        = motion["before"].toInt();
            camera.afterS         = motion["after"].toInt();
            camera.minThreshold   = motion["min"].toDouble();
            camera.maxThreshold   = motion["max"].toDouble();
            camera.motionDuration = motion["duration"].toDouble();
            camera.motionQuota    = motion["quota"].toInt();
        }
        if (obj.contains("noise")) {
            const auto noise = obj["noise"].toObject();

            camera.minNoise = noise["min"].toDouble();
            camera.maxNoise = noise["max"].toDouble();
        }
        if (obj.contains("region")) {
            const auto region = obj["region"].toObject();

            camera.regionEnabled = region["enabled"].toBool();
            camera.regionX       = region["x"].toInt();
            camera.regionY       = region["y"].toInt();
            camera.regionWidth   = region["width"].toInt();
            camera.regionHeight  = region["height"].toInt();
        }
        if (obj.contains("frames")) {
            const auto frames = obj["frames"].toObject();

            camera.framesToStep    = frames["step"].toInt();
            camera.framesToSegment = frames["segment"].toInt();
            camera.shiftCount      = frames["shift"].toInt();
        }
        if (obj.contains("segments")) {
            const auto segments = obj["segments"].toObject();

            camera.segmentsToStart = segments["start"].toInt();
            camera.segmentsToEnd   = segments["end"].toInt();
            camera.segmentDuration = segments["duration"].toDouble();
        }
        if (obj.contains("alternative")) {
            const auto alternative = obj["alternative"].toObject();

            camera.alternative    = alternative["enabled"].toBool();
            camera.alternativeUrl = alternative["url"].toString();
        }
        if (obj.contains("onvif")) {
            const auto onvif = obj["onvif"].toObject();

            camera.onvifEnabled  = onvif["enabled"].toBool();
            camera.onvifPath     = onvif["path"].toString();
            camera.onvifHostname = onvif["hostname"].toString();
            camera.onvifUsername = onvif["username"].toString();
            camera.onvifPassword = onvif["password"].toString();
            camera.onvifPort     = onvif["port"].toInt();
            camera.onvifQuota    = onvif["quota"].toInt();
            camera.onvifDelay    = onvif["delay"].toDouble();
        }

        sources.append(camera);
    }
}

void Settings::save() {
    QJsonDocument doc;
    QJsonObject   root;

    QFile file(QDir::homePath() + confPath);

    if (!file.open(QFile::WriteOnly)) {
        qDebug("Failed to write config file");
        return;
    }

    root.insert("port", port);
    root.insert("login", login);
    root.insert("passwd", passwd);

    QJsonArray sourcesArr;

    for (const auto & camera : qAsConst(sources)) {
        QJsonObject obj;

        obj.insert("enabled", camera.enabled);
        obj.insert("url", camera.url);
        obj.insert("ext", camera.ext);

        QJsonObject audio;
        audio.insert("enabled", camera.audioEnabled);
        obj.insert("audio", audio);

        QJsonObject persistent;
        persistent.insert("enabled", camera.persistentEnabed);
        persistent.insert("path", camera.persistentPath);
        persistent.insert("quota", camera.persistentQuota);
        obj.insert("persistent", persistent);

        QJsonObject motion;
        motion.insert("enabled", camera.motionEnabled);
        motion.insert("path", camera.motionPath);
        motion.insert("before", camera.beforeS);
        motion.insert("after", camera.afterS);
        motion.insert("min", camera.minThreshold);
        motion.insert("max", camera.maxThreshold);
        motion.insert("duration", camera.motionDuration);
        motion.insert("quota", camera.motionQuota);
        obj.insert("motion", motion);

        QJsonObject noise;
        noise.insert("min", camera.minNoise);
        noise.insert("max", camera.maxNoise);
        obj.insert("noise", noise);

        QJsonObject region;
        region.insert("enabled", camera.regionEnabled);
        region.insert("x", camera.regionX);
        region.insert("y", camera.regionY);
        region.insert("width", camera.regionWidth);
        region.insert("height", camera.regionHeight);
        obj.insert("region", region);

        QJsonObject frames;
        frames.insert("step", camera.framesToStep);
        frames.insert("segment", camera.framesToSegment);
        frames.insert("shift", camera.shiftCount);
        obj.insert("frames", frames);

        QJsonObject segments;
        segments.insert("start", camera.segmentsToStart);
        segments.insert("end", camera.segmentsToEnd);
        segments.insert("duration", camera.segmentDuration);
        obj.insert("segments", segments);

        QJsonObject alternative;
        alternative.insert("enabled", camera.alternative);
        alternative.insert("url", camera.alternativeUrl);
        obj.insert("alternative", alternative);

        QJsonObject onvif;
        onvif.insert("enabled", camera.onvifEnabled);
        onvif.insert("path", camera.onvifPath);
        onvif.insert("hostname", camera.onvifHostname);
        onvif.insert("username", camera.onvifUsername);
        onvif.insert("password", camera.onvifPassword);
        onvif.insert("port", camera.onvifPort);
        onvif.insert("quota", camera.onvifQuota);
        onvif.insert("delay", camera.onvifDelay);
        obj.insert("onvif", onvif);

        sourcesArr.append(obj);
    }

    root.insert("sources", sourcesArr);
    doc.setObject(root);

    file.write(doc.toJson(QJsonDocument::Indented));
    file.close();
    createNodeModule();

    qDebug("Settings saved successfully");
}

void Settings::createNodeModule() {
    QString dir = QDir::homePath() + '/' + appDir + '/' + "node";

    QDir root{"/"};
    root.mkdir(dir);

    QFile read1(":/index.js");
    QFile read2(":/package.json");
    QFile write1(dir + "/index.js");
    QFile write2(dir + "/package.json");

    read1.open(QFile::ReadOnly);
    read2.open(QFile::ReadOnly);
    write1.open(QFile::WriteOnly);
    write2.open(QFile::WriteOnly);

    write1.write(read1.readAll());
    write2.write(read2.readAll());

    read1.close();
    read2.close();
    write1.close();
    write2.close();
}
