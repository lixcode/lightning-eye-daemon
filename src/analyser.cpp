#include "analyser.h++"

#include <QDebug>
#include <QDir>
#include <QProcess>
#include <QRegularExpression>
#include <QTextStream>

Analyser::Analyser() {}

QList<Score> Analyser::run(const Camera & cam, QString path) {
    QString bufferPath =
      QDir::homePath() + '/' + appDir + "/buffer" + QString::number(cam.id);
    QProcess     process;
    QList<Score> ret;

    if (cam.alternative) {
        path.replace(cam.ext, "-a" + cam.ext);
    }

    process.setProgram("ffmpeg");
    process.setArguments(
      {"-i", path, "-vf", "-filter_threads", "1",
       (cam.framesToStep > 1
          ? "select='not(mod(n\\," + QString::number(cam.framesToStep) + "))',"
          : "") +
         (cam.regionEnabled ? "crop=" + QString::number(cam.regionWidth) + ':' +
                                QString::number(cam.regionHeight) + ':' +
                                QString::number(cam.regionX) + ':' +
                                QString::number(cam.regionY) + ','
                            : "") +
         "select='gte(scene\\,0)',metadata=print:file=" + bufferPath,
       "-an", "-f", "null", "-"});

    qDebug(
      "run analyze ffmpeg %s", process.arguments().join(" ").toUtf8().data());

    process.start();
    process.waitForStarted();
    process.waitForFinished(30 * 60 * 1000);

    if (process.exitCode() != 0) {
        return ret;
    }

    QFile       buffer{bufferPath};
    QTextStream stream{&buffer};

    if (!buffer.open(QFile::ReadOnly)) {
        return ret;
    }

    QRegularExpression line1exp{"pts_time:(?<time>\\d+(\\.\\d+)?)"};
    QRegularExpression line2exp{"scene_score=(?<score>\\d+(\\.\\d+)?)"};

    while (!stream.atEnd()) {
        auto line1  = stream.readLine();
        auto line2  = stream.readLine();
        auto check1 = line1exp.match(line1);
        auto check2 = line2exp.match(line2);

        if (check1.hasMatch() && check2.hasMatch()) {
            auto time  = check1.captured("time").toFloat();
            auto score = check2.captured("score").toFloat();

            ret.append({time, score});
        }
    }

    buffer.close();
    buffer.remove();

    if (cam.framesToSegment != 1) {
        QList<Score> combined;

        auto frames = cam.framesToSegment;

        for (int i = 0; i < ret.length() - frames + 1; i += frames) {
            float score = 0;

            for (int j = 0; j < frames; j++) {
                score += ret[i + j].score;
            }

            combined.append({ret[i].time, score / float(frames)});
        }

        ret = combined;
    }

    return ret;
}
