#ifndef MOTION_H
#define MOTION_H

#include "settings.h++"

#include <QString>


struct Interval
{
    float begin;
    float end;
};

class Motion
{
public:
    Motion();

    static QList<Interval> run(const Camera & cam, const QString & path);
};

#endif  // MOTION_H
