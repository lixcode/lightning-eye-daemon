#include "dir.h++"

#include "daemon.h++"
#include "ftp.h++"
#include "onvifextractor.h++"

#include <QDebug>
#include <QMutex>
#include <QRegularExpression>
#include <QStorageInfo>
#include <QTextStream>
#include <QThread>
#include <QTimer>

Dir::Dir(QObject * parent)
    : parent(parent) {}

QString Dir::path() const {
    return mpath;
}

void Dir::setPath(const QString & path) {
    QRegularExpression ftpEx{
      "^s?ftp://"
      "(?<login>\\w+):(?<password>\\w+)@(?<host>[\\w\\.]+):(?<port>\\d+)/"
      "(?<path>.*)$"};

    auto match = ftpEx.match(path);

    if (match.hasMatch()) {
        login    = match.captured("login");
        password = match.captured("password");
        host     = match.captured("host");
        port     = match.captured("port");
        ipath    = match.captured("path");

        ftp = new Ftp();

        ftp->initSession(
          host, port.toInt(), login, password,
          path[0] == 's' ? Ftp::FTP_PROTOCOL::SFTP : Ftp::FTP_PROTOCOL::FTP);

        mode = Modes::Ftp;
        __init();
    }
    else {
        ipath = path;
        dir.setPath(path);
        _init();
    }

    mpath = QString{path}.replace(QRegExp{"/$"}, "");
    fpath = path;
}

bool Dir::mkpath(QString path) {
    path.remove(fpath);

    if (mode == Modes::Local) {
        return _mkPath(path);
    }
    else {
        return __mkPath(path);
    }
}

bool Dir::rmDir(QString path) {
    path.remove(fpath);

    if (mode == Modes::Local) {
        return _rmDir(path);
    }
    else {
        return __rmDir(path);
    }
}

bool Dir::rmFile(QString path, bool internal) {
    path.remove(fpath);

    if (!internal) {
        auto hDir = findH(path);
        auto name = path.split('/').last();

        QList<int> toRemove;

        for (int i = hDir->files.length() - 1; i >= 0; i--) {
            auto file = hDir->files[i];

            if (file.name == name) {
                toRemove.append(i);
                size -= file.size;
            }
        }

        for (auto & i : toRemove)
            hDir->files.removeAt(i);
    }

    if (mode == Modes::Local) {
        return _rmFile(path);
    }
    else {
        return __rmFile(path);
    }
}

bool Dir::addFile(QString path) {
    path.remove(fpath);

    auto names = path.split('/');


    auto h = findH(path);
    auto f = check(h->files, names.last());

    if (!f) {
        SFile file{names.last()};

        if (mode == Modes::Local) {
            QFileInfo info(ipath + path);

            file.size = info.size();
            f         = &file;
        }
        else {
            auto list = ftp->list(
              ipath + names[0] + '/' + names[1] + '/' + names[2] + '/', '-');

            f = check(list, file.name);

            if (f) {
                file.size = f->size;
            }
        }

        h->files.append(file);
        size += file.size;
    }

    return f != nullptr;
}

void Dir::setQuota(qint64 value) {
    quota = value;
}

void Dir::ensureSpace() {

    qint64 amo      = 1024 * 1024 * 512;
    qint64 toRemove = 0;
    int    counter  = 0;

    if (quota == 0 && mode == Modes::Local) {
        QStorageInfo storage(dir);
        toRemove = amo - storage.bytesAvailable();
    }
    else if (quota > 0) {
        toRemove = amo - (quota - size);
    }

    if (toRemove <= amo / 2) {
        return;
    }

    for (int i = 0; i < dirs.length(); i++) {
        auto & ymDir = dirs[i];

        QList<int> dR;
        for (int j = 0; j < ymDir.dirs.length(); j++) {
            auto & dDir = ymDir.dirs[j];

            if (
              ymDir.name == dirs.last().name &&
              dDir.name == ymDir.dirs.last().name) {
                break;
            }

            QList<int> hR;
            for (int k = 0; k < dDir.dirs.length(); k++) {
                auto & hDir = dDir.dirs[k];

                if (toRemove > 0) {
                    QList<int> fR;
                    for (int l = 0; l < hDir.files.length(); l++) {
                        auto fileInfo = hDir.files[l];

                        toRemove -= fileInfo.size;
                        size -= fileInfo.size;
                        rmFile(
                          ymDir.name + '/' + dDir.name + '/' + hDir.name + '/' +
                            fileInfo.name,
                          true);
                        counter++;

                        fR.prepend(i);

                        if (toRemove < 0) {
                            break;
                        }
                    }

                    for (int & l : fR) {
                        hDir.files.removeAt(l);
                    }
                }

                if (hDir.files.isEmpty()) {
                    rmDir(ymDir.name + '/' + dDir.name + '/' + hDir.name);
                    hR.prepend(k);
                }
            }
            for (int & k : hR) {
                dDir.dirs.removeAt(k);
            }

            if (dDir.dirs.isEmpty()) {
                rmDir(ymDir.name + '/' + dDir.name);
                dR.prepend(j);
            }
        }
        for (int & j : dR) {
            ymDir.dirs.removeAt(j);
        }

        if (ymDir.dirs.isEmpty()) {
            rmDir(ymDir.name);
        }
    }
    qDebug("cleared %i file(s) from %s", counter, mpath.toUtf8().constData());
}

QDateTime Dir::creationTime(QString path) {
    if (mode == Modes::Local) {
        QFileInfo info(path);

        return info.birthTime();
    }
    else {
        QRegularExpression parser{
          "/(\\d+)-(\\d+)/(\\d+)/(\\d+)/(\\d+)-(\\d+)\\."};
        auto match = parser.match(path);

        if (match.hasMatch()) {
            return OnvifExtractor::extractDate(match);
        }
    }
    return {};
}

bool Dir::checkForMissing(QStringList & in, QStringList & ain) {
    auto files  = mode == Modes::Local ? _missings() : __missings();
    auto regex  = QRegExp("\\d{4}-\\d{2}/\\d{2}/\\d{2}/\\d{2}-\\d{2}\\.\\w+");
    auto aregex = QRegExp("\\d{4}-\\d{2}/\\d{2}/\\d{2}/\\d{2}-\\d{2}-a\\.\\w+");

    for (auto & name : files) {
        if (regex.exactMatch(name)) {
            in.append(name);
        }
        else if (aregex.exactMatch(name)) {
            ain.append(name);
        }
    }

    return !files.isEmpty();
}

void Dir::_init() {
    auto ymList =
      dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

    for (int i = 0; i < ymList.length(); i++) {
        QDir  ymDir{ymList[i].absoluteFilePath()};
        YmDir ymS{ymList[i].completeBaseName()};
        auto  dList =
          ymDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        for (int j = 0; j < dList.length(); j++) {
            QDir dDir{dList[j].absoluteFilePath()};
            DDir dS{dList[j].completeBaseName()};
            auto hList =
              dDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

            for (int k = 0; k < hList.length(); k++) {
                QDir hDir{hList[k].absoluteFilePath()};
                HDir hS{hList[k].completeBaseName()};
                auto fList = hDir.entryInfoList(
                  QDir::Files | QDir::NoSymLinks, QDir::Name);

                for (auto & fileInfo : fList) {
                    hS.files.append(
                      {fileInfo.completeBaseName(), fileInfo.size()});
                    size += fileInfo.size();
                }

                dS.dirs.append(hS);
            }

            ymS.dirs.append(dS);
        }

        dirs.append(ymS);
    }
}

void Dir::__init() {
    auto ymList = ftp->list(ipath, 'd');

    for (int i = 0; i < ymList.length(); i++) {
        YmDir ymS{ymList[i].name};
        auto  dList = ftp->list(ipath + ymS.name + '/', 'd');

        for (int j = 0; j < dList.length(); j++) {
            DDir dS{dList[j].name};
            auto hList = ftp->list(ipath + ymS.name + '/' + dS.name + '/', 'd');

            for (int k = 0; k < hList.length(); k++) {
                HDir hS{hList[k].name};
                auto fList = ftp->list(
                  ipath + ymS.name + '/' + dS.name + '/' + hS.name + '/', '-');

                for (auto & fileInfo : fList) {
                    hS.files.append({fileInfo.name, fileInfo.size});
                    size += fileInfo.size;
                }

                dS.dirs.append(hS);
            }

            ymS.dirs.append(dS);
        }

        dirs.append(ymS);
    }

    qDebug() << "ftp folder inited" << this->ipath << this->size;
}

QStringList Dir::_missings() {
    QStringList ret;
    auto        ymList =
      dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

    for (auto & ym : ymList) {
        QDir ymDir{ym.absoluteFilePath()};
        auto ymS = check(dirs, {ym.completeBaseName()});
        auto dList =
          ymDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        if (!ymS) {
            dirs.append({ym.completeBaseName()});
            ymS = &dirs.last();
        }

        for (auto & d : dList) {
            QDir dDir{d.absoluteFilePath()};
            auto dS = check(ymS->dirs, d.completeBaseName());
            auto hList =
              dDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

            if (!dS) {
                ymS->dirs.append({d.completeBaseName()});
                dS = &ymS->dirs.last();
            }

            for (auto & h : hList) {
                QDir hDir{h.absoluteFilePath()};
                auto hS    = check(dS->dirs, h.completeBaseName());
                auto fList = hDir.entryInfoList(
                  QDir::Files | QDir::NoSymLinks, QDir::Name);

                if (!hS) {
                    dS->dirs.append({h.completeBaseName()});
                    hS = &dS->dirs.last();
                }

                for (auto & f : fList) {

                    auto fS = check(hS->files, f.completeBaseName());

                    if (!fS && f.size() > 1024 * 1024) {
                        hS->files.append({f.completeBaseName(), f.size()});

                        size += f.size();
                        ret.append(f.absoluteFilePath());
                    }
                }
            }
        }
    }

    ret.replaceInStrings(fpath, "");

    return ret;
}

QStringList Dir::__missings() {
    QStringList ret;
    auto        ymList = ftp->list(ipath, 'd');

    for (auto & ym : ymList) {
        auto ymS   = check(dirs, ym.name);
        auto dList = ftp->list(ipath + ym.name + '/', 'd');

        if (!ymS) {
            dirs.append({ym.name});
            ymS = &dirs.last();
        }

        for (auto & d : dList) {
            auto dS    = check(ymS->dirs, d.name);
            auto hList = ftp->list(ipath + ym.name + '/' + d.name + '/', 'd');

            if (!dS) {
                ymS->dirs.append({d.name});
                dS = &ymS->dirs.last();
            }

            for (auto & h : hList) {
                auto hS    = check(dS->dirs, h.name);
                auto fList = ftp->list(
                  ipath + ym.name + '/' + d.name + '/' + h.name + '/', '-');

                if (!hS) {
                    dS->dirs.append({h.name});
                    hS = &dS->dirs.last();
                }

                for (auto & f : fList) {
                    auto fS = check(hS->files, f.name);

                    if (!fS && f.size > 1024 * 1024) {
                        hS->files.append({f.name, f.size});
                        size += f.size;

                        ret.append(
                          ym.name + '/' + d.name + '/' + h.name + '/' + f.name);
                    }
                }
            }
        }
    }

    return ret;
}

bool Dir::_mkPath(const QString & path) {
    return dir.mkpath(path);
}

bool Dir::__mkPath(const QString & path) {
    auto names = path.split('/');
    auto index = 0;

    auto level1 = check(dirs, names[0]);
    if (level1 != nullptr) {
        index = 1;

        auto level2 = check(level1->dirs, names[1]);
        if (level2 != nullptr) {
            index = 2;

            auto level3 = check(level2->dirs, names[2]);
            if (level3 != nullptr) {
                return true;
            }
        }
    }

    return ftp->createDir(
      ipath + (index == 0 ? "" : names.mid(0, index).join('/') + '/'),
      names.mid(index, 3 - index).join('/'));
}

bool Dir::_rmDir(const QString & path) {
    return dir.remove(path);
}

bool Dir::__rmDir(const QString & path) {
    return ftp->removeDir(ipath + path);
}

bool Dir::_rmFile(const QString & path) {
    return dir.remove(path);
}

bool Dir::__rmFile(const QString & path) {
    return ftp->removeFile(ipath + path);
}

Dir::HDir * Dir::findH(QString path) {
    path.remove(fpath);

    auto names = path.split('/');

    auto ym = check(dirs, names[0]);
    if (!ym) {
        dirs.append({names[0]});
        ym = &dirs.last();
    }

    auto d = check(ym->dirs, names[1]);
    if (!d) {
        ym->dirs.append({names[1]});
        d = &ym->dirs.last();
    }

    auto h = check(d->dirs, names[2]);
    if (!h) {
        d->dirs.append({names[3]});
        h = &d->dirs.last();
    }

    return h;
}

template <typename T>
T * Dir::check(QList<T> & list, const QString & name) {
    for (int i = 0; i < list.length(); i++) {
        auto & value = list[i];
        if (value.name == name) {
            return &value;
        }
    }

    return nullptr;
}
