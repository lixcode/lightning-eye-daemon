#ifndef ONVIFEXTRACTOR_H
#define ONVIFEXTRACTOR_H

#include "dir.h++"
#include "settings.h++"

#include <QDateTime>
#include <QFile>
#include <QList>


struct Fragment
{
    QDateTime begin;
    QDateTime end;
};


class OnvifExtractor
{
    QList<Fragment> fragments;

    QFile file;

    QDateTime beginTime;

    bool lastWasOn = false;

public:
    OnvifExtractor(const QString & path);

    void run(
      const Camera & cam, Dir & dir, const QString & path, QString outputPath);

    void reopenFile();

    static QDateTime extractDate(const QRegularExpressionMatch & match);
};

#endif  // ONVIFEXTRACTOR_H
