#ifndef ANALYSER_H
#define ANALYSER_H

#include "settings.h++"

#include <QList>



struct Score
{
    float time;
    float score;
};

class Analyser
{
public:
    Analyser();

    static QList<Score> run(const Camera & cam, QString path);
};

#endif  // ANALYSER_H
