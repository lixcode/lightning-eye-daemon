#include "motion.h++"

#include "analyser.h++"

#include <QDebug>
#include <QtMath>

Motion::Motion() {}

QList<Interval> Motion::run(const Camera & cam, const QString & path) {
    QList<Interval> tmp, ret;

    auto score = Analyser::run(cam, path);

    auto motionCount   = 0;
    auto noMotionCount = 0;
    auto findMotion    = true;
    auto beginTime     = 0.f;

    for (int i = cam.shiftCount; i < score.length(); i++) {
        auto frameScore = score[i].score;
        bool isMotionFrame =
          frameScore >= cam.minThreshold && frameScore <= cam.maxThreshold &&
          !(frameScore >= cam.minNoise && frameScore <= cam.maxNoise);

        if (isMotionFrame) {
            motionCount++;
            noMotionCount = 0;

            if (findMotion && motionCount == cam.segmentsToStart) {
                beginTime  = score[i - cam.segmentsToStart + 1].time;
                findMotion = false;
            }
        }
        else {
            noMotionCount++;
            motionCount = 0;

            if (!findMotion && noMotionCount == cam.segmentsToEnd) {
                tmp.append(
                  {qMax(beginTime - cam.beforeS, 0.f),
                   score[i].time + cam.afterS});
                findMotion = true;
            }
        }
    }

    if (tmp.length() > 0) {
        auto current = tmp[0];

        for (int i = 1; i < tmp.length(); i++) {
            const auto & canditate = tmp[i];

            if (canditate.begin - current.end > 0) {
                ret.append(current);
                current = canditate;
            }
            else {
                current.end = canditate.end;
            }
        }
        ret.append(current);
    }

    return ret;
}
