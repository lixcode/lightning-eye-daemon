#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include "dir.h++"
#include "motion.h++"
#include "settings.h++"

#include <QList>



class Extractor
{
public:
    Extractor();

    static void run(
      const Camera & cam, Dir & dir, const QString & path, QString outputPath);

    static void extract(
      const QList<Interval> & intervals, const Camera & cam, Dir & dir,
      const QString & path, QString outputPath);

private:
    static QString formatInt(const QString & in);
};

#endif  // EXTRACTOR_H
